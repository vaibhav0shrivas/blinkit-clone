
//userReducer
export const SIGN_IN_USER = "SIGN_IN_USER";
export const SIGN_OUT_USER = "SIGN_OUT_USER";
export const ADD_ADDRESS = "ADD_ADDRESS";
export const REMOVE_ADDRESS = "REMOVE_ADDRESS";
export const UPDATE_ADDRESS = "UPDATE_ADDRESS";

//locationReducer
export const SET_LOCATION = "SET_LOCATION";

//ProductReducer
export const LOAD_PRODUCT_DATA = "LOAD_PRODUCT_DATA";

//CartReducer
export const ADD_PRODUCT_TO_CART = "ADD_PRODUCT_TO_CART";
export const REMOVE_PRODUCT_FROM_CART = "REMOVE_PRODUCT_FROM_CART";

//searchReducer
export const UPDATE_SEARCH_STRING = "UPDATE_SEARCH_STRING";

