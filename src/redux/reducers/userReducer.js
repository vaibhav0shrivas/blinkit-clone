import {
    SIGN_IN_USER,
    SIGN_OUT_USER,
    ADD_ADDRESS,
    REMOVE_ADDRESS,
    UPDATE_ADDRESS,
} from "../actionTypes";

const initialState = {
    user: {
        isLoggedIn: false,
        userId: null,
        userPhoneNumber: null,
        userAddresses: [
            /*{
                addressesNickName:"Home",
                receiverTitle:"Miss/Mrs/Mr",
                receiverName:"full name",
                addressOfHouse:"",
                addressOfStreet:"",
                latitude:<float>,
                longitude:<float>,
            }*/
        ]
    },
};

const userReducer = (state = initialState, action) => {
    let updatedState;
    let updatedUserAddresses;
    switch (action.type) {

        case ADD_ADDRESS:
            updatedState = {
                ...state,
                user: {
                    ...state.user,
                    userAddresses: [
                        ...state.user.userAddresses,
                        {
                            addressesNickName: action.payload.addressesNickName,
                            receiverTitle: action.payload.receiverTitle,
                            receiverName: action.payload.receiverName,
                            addressOfHouse: action.payload.addressOfHouse,
                            addressOfStreet: action.payload.addressOfStreet,
                            latitude: action.payload.latitude,
                            longitude: action.payload.longitude,
                        }


                    ]
                }

            }
            return updatedState;

        case REMOVE_ADDRESS:
            updatedUserAddresses = state.user.userAddresses;
            updatedUserAddresses = updatedUserAddresses.filter((addressObject) => {
                if (action.payload.addressesNickName === addressObject) {
                    return false;
                } else {
                    return true;
                }
            })
            
            updatedState = {
                ...state,
                user: {
                    ...state.user,
                    userAddresses: [
                        ...updatedUserAddresses
                    ]
                }

            }
            return updatedState;

        case UPDATE_ADDRESS:
            updatedUserAddresses = state.user.userAddresses;
            updatedUserAddresses = updatedUserAddresses.map((addressObject) => {
                if (action.payload.addressesNickName === addressObject) {
                    return action.payload;
                } else {
                    return addressObject;
                }
            });

            updatedState = {
                ...state,
                user: {
                    ...state.user,
                    userAddresses: [
                        ...updatedUserAddresses
                    ]
                }

            }

            return updatedState;

        case SIGN_OUT_USER:
            updatedState = {
                ...state,
                user: {
                    ...state.user,
                    isLoggedIn: false,
                    userId: null,
                    userPhoneNumber: null,
                    userAddresses: [],
                }

            }
            return updatedState;

        case SIGN_IN_USER:
            updatedState = {
                ...state,
                user: {
                    ...state.user,
                    isLoggedIn: action.payload.isLoggedIn,
                    userId: action.payload.userId,
                    userPhoneNumber: action.payload.userPhoneNumber,
                    userAddresses: [
                        {
                            addressesNickName: "Home",
                            receiverTitle: "Mr",
                            receiverName: "John Doe",
                            addressOfHouse: "34 st 342/12",
                            addressOfStreet: "Kormangla Road",
                            latitude: 73.5,
                            longitude: 22.8,
                        }
                    ],
                }

            }
            return updatedState;
        default:
            return state;
    };
}

export default userReducer;