import {
    UPDATE_SEARCH_STRING,
} from "../actionTypes";

const initialState = {
    search: {
        searchString: "",
    },
};

const searchReducer = (state = initialState, action) => {
    let updatedState;
    switch (action.type) {

        case UPDATE_SEARCH_STRING:
            updatedState = {
                ...state,
                search: {
                    searchString: action.payload.searchString,
                }

            }
            return updatedState;
        default:
            return state;
    };
}

export default searchReducer;