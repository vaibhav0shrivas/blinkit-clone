import {
    ADD_PRODUCT_TO_CART,
    REMOVE_PRODUCT_FROM_CART,
} from "../actionTypes";

const initialState = {
    cartItems: {
        cartTotalItems: 0,
        cartTotalPrice: 0

    }
};

const cartReducer = (state = initialState, action) => {
    let updatedState;
    let newProductQuantity;
    let newCartTotalItems;
    let newCartTotalPrice;
    switch (action.type) {

        case ADD_PRODUCT_TO_CART:

            if (action.payload.productId in state.cartItems) {
                newProductQuantity = state.cartItems[action.payload.productId] + 1;
            } else {
                newProductQuantity = 1;
            }
            newCartTotalItems = state.cartItems.cartTotalItems + 1;
            newCartTotalPrice = state.cartItems.cartTotalPrice + action.payload.productPrice;


            updatedState = {
                cartItems: {
                    ...state.cartItems,
                    cartTotalItems: newCartTotalItems,
                    cartTotalPrice: newCartTotalPrice,
                    [action.payload.productId]: newProductQuantity,
                }
            }
            return updatedState;

        case REMOVE_PRODUCT_FROM_CART:

            if (action.payload.productId in state.cartItems &&
                (state.cartItems[action.payload.productId] - 1) >= 1) {

                newProductQuantity = state.cartItems[action.payload.productId] - 1;
                newCartTotalItems = state.cartItems.cartTotalItems - 1;
                newCartTotalPrice = state.cartItems.cartTotalPrice - action.payload.productPrice;

                updatedState = {
                    cartItems: {
                        ...state.cartItems,
                        cartTotalItems: newCartTotalItems,
                        cartTotalPrice: newCartTotalPrice,
                        [action.payload.productId]: newProductQuantity,
                    }
                }
                return updatedState;

            } else if (action.payload.productId in state.cartItems &&
                state.cartItems[action.payload.productId] === 1) {

                newCartTotalItems = state.cartItems.cartTotalItems - 1;
                newCartTotalPrice = state.cartItems.cartTotalPrice - action.payload.productPrice;

                updatedState = {
                    cartItems: {
                        ...state.cartItems,
                        cartTotalItems: newCartTotalItems,
                        cartTotalPrice: newCartTotalPrice,
                    }
                }
                delete updatedState.cartItems[action.payload.productId];

                return updatedState;

            } else {
                console.error("userReducer.REMOVE_PRODUCT_FROM_CART called on item that does not exist in cart.");
                return state;
            }

        default:
            return state;
    };
}

export default cartReducer;