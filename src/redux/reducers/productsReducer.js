import { LOAD_PRODUCT_DATA } from "../actionTypes";

const staticProductsData = [
    {
        "id": "1",
        "title": "Amul Pure Milk Cheese Slices",
        "images": [
            "https://cdn.grofers.com/cdn-cgi/image/f=auto,fit=scale-down,q=50,metadata=none,w=135/app/images/products/sliding_image/168a.jpg",
        ],
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "brand": "Amul",
        "quantity": {
            count: 200,
            unit: "g"
        },
        "price": 130,
        "discount": 0.10,
        "category": "Dairy, Bread & Eggs",
        "keyFeatures": [
            "100% Vegetarian",
            "Lip smacking flavour",
            "High quality source of milk and proteins",
            "Contains 10 slices",
        ],
        "contents": "10 slices",
        "shelfLife": "6 months",
        "manufacturerDetails": "Kaira District Co-operative Milk Producers' Union Limited, Anand 388 001. At Food Complex Mogar, Mogar. Lic. No. - 10014021001010",
        "license_FSSAILicense": "10012021000071",
        "expiryDate": "Please refer to the packaging of the product for expiry date",
        "seller": "LA SUPER RETAIL PRIVATE LIMITED",
    },
    {
        "id": "2",
        "title": "Nandini Toned Fresh Milk",
        "images": [
            "https://cdn.grofers.com/cdn-cgi/image/f=auto,fit=scale-down,q=50,metadata=none,w=135/app/images/products/sliding_image/37083a.jpg?ts=1651131853"
        ],
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "brand": "Nandini",
        "quantity": {
            count: 500,
            unit: "ml"
        },
        "price": 24,
        "discount": 0,
        "category": "Dairy, Bread & Eggs",
        "keyFeatures": [
            "Healthy and fresh",
            "Used in making tea and coffee, etc"
        ],
        "shelfLife": "2 Days",
        "manufacturerDetails": "Kaira District Co-operative Milk Producers' Union Limited, Anand 388 001. At Food Complex Mogar, Mogar. Lic. No. - 10014021001010",
        "license_FSSAILicense": "10012021000071",
        "expiryDate": "Please refer to the packaging of the product for expiry date",
        "seller": "LA SUPER RETAIL PRIVATE LIMITED",
    },
    {
        "id": "3",
        "title": "Amul Salted Butter",
        "images": [
            "https://cdn.grofers.com/cdn-cgi/image/f=auto,fit=scale-down,q=50,metadata=none,w=135/app/images/products/sliding_image/160a.jpg?ts=1654778815"],
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "brand": "Amul",
        "quantity": {
            count: 500,
            unit: "g"
        },
        "price": 265,
        "discount": 0,
        "category": "Dairy, Bread & Eggs",
        "keyFeatures": [
            "100% Vegetarian",
            "Lip smacking flavour",
        ],
        "shelfLife": "6 months",
        "manufacturerDetails": "Kaira District Co-operative Milk Producers' Union Limited, Anand 388 001. At Food Complex Mogar, Mogar. Lic. No. - 10014021001010",
        "license_FSSAILicense": "10012021000071",
        "expiryDate": "Please refer to the packaging of the product for expiry date",
        "seller": "LA SUPER RETAIL PRIVATE LIMITED",
    },
    {
        "id": "4",
        "title": "Jaya White Eggs",
        "images": [
            "https://cdn.grofers.com/cdn-cgi/image/f=auto,fit=scale-down,q=50,metadata=none,w=135/app/images/products/sliding_image/480950a.jpg?ts=1650087748"],
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "brand": "Jaya",
        "quantity": {
            count: 6,
            unit: "pieces"
        },
        "price": 65,
        "discount": 0.3,
        "category": "Dairy, Bread & Eggs",
        "keyFeatures": [
            "Lip smacking flavour",
        ],
        "shelfLife": "21 days",
        "manufacturerDetails": "Kaira District Co-operative Milk Producers' Union Limited, Anand 388 001. At Food Complex Mogar, Mogar. Lic. No. - 10014021001010",
        "license_FSSAILicense": "10012021000071",
        "expiryDate": "Please refer to the packaging of the product for expiry date",
        "seller": "LA SUPER RETAIL PRIVATE LIMITED",
    },
    {
        "id": "5",
        "title": "Britannia 100% Whole Wheat Bread",
        "images": [
            "https://cdn.grofers.com/cdn-cgi/image/f=auto,fit=scale-down,q=50,metadata=none,w=135/app/images/products/sliding_image/127031a.jpg?ts=1652006846"],
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "brand": "Britannia",
        "quantity": {
            count: 400,
            unit: "g"
        },
        "price": 55,
        "discount": 0,
        "category": "Dairy, Bread & Eggs",
        "keyFeatures": [
            "100% Vegetarian",
            "Made with 100% whole wheat grains",
            "No trans fat",
        ],
        "shelfLife": "5 days",
        "manufacturerDetails": "Kaira District Co-operative Milk Producers' Union Limited, Anand 388 001. At Food Complex Mogar, Mogar. Lic. No. - 10014021001010",
        "license_FSSAILicense": "10012021000071",
        "expiryDate": "Please refer to the packaging of the product for expiry date",
        "seller": "LA SUPER RETAIL PRIVATE LIMITED",
    },
    {
        "id": "6",
        "title": "ACT II Golden Sizzle Popcorn",
        "images": [
            "https://cdn.grofers.com/cdn-cgi/image/f=auto,fit=scale-down,q=50,metadata=none,w=135/app/images/products/sliding_image/54056a.jpg?ts=1660032971"],
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "brand": "ACT II",
        "quantity": {
            count: 20,
            unit: "g"
        },
        "price": 30,
        "discount": 0.10,
        "category": "Snacks and Munchie",
        "keyFeatures": [
            "100% Vegetarian",
            "Lip smacking flavour",
            "High quality source of milk and proteins",
            "Contains 10 slices",
        ],
        "shelfLife": "6 months",
        "manufacturerDetails": "Kaira District Co-operative Milk Producers' Union Limited, Anand 388 001. At Food Complex Mogar, Mogar. Lic. No. - 10014021001010",
        "license_FSSAILicense": "10012021000071",
        "expiryDate": "Please refer to the packaging of the product for expiry date",
        "seller": "LA SUPER RETAIL PRIVATE LIMITED",
    },
    {
        "id": "7",
        "title": "Lay's Hot n sweet Chilli Flavour Chips",
        "images": [
            "https://cdn.grofers.com/cdn-cgi/image/f=auto,fit=scale-down,q=50,metadata=none,w=135/app/images/products/sliding_image/289152a.jpg?ts=1668146939"],
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "brand": "Lay's",
        "quantity": {
            count: 50,
            unit: "g"
        },
        "price": 50,
        "discount": 0,
        "category": "Snacks and Munchie",
        "keyFeatures": [
            "Healthy and fresh",
            "Used in making tea and coffee, etc"
        ],
        "shelfLife": "1 year",
        "manufacturerDetails": "Kaira District Co-operative Milk Producers' Union Limited, Anand 388 001. At Food Complex Mogar, Mogar. Lic. No. - 10014021001010",
        "license_FSSAILicense": "10012021000071",
        "expiryDate": "Please refer to the packaging of the product for expiry date",
        "seller": "LA SUPER RETAIL PRIVATE LIMITED",
    },
    {
        "id": "8",
        "title": "Doritos Cheese Nachos",
        "images": [
            "https://cdn.grofers.com/cdn-cgi/image/f=auto,fit=scale-down,q=50,metadata=none,w=135/app/images/products/sliding_image/432818a.jpg?ts=1669469997"],
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "brand": "Doritos",
        "quantity": {
            count: 60,
            unit: "g"
        },
        "price": 95,
        "discount": 0,
        "category": "Snacks and Munchie",
        "keyFeatures": [
            "100% Vegetarian",
            "Lip smacking flavour",
        ],
        "shelfLife": "9 months",
        "manufacturerDetails": "Kaira District Co-operative Milk Producers' Union Limited, Anand 388 001. At Food Complex Mogar, Mogar. Lic. No. - 10014021001010",
        "license_FSSAILicense": "10012021000071",
        "expiryDate": "Please refer to the packaging of the product for expiry date",
        "seller": "LA SUPER RETAIL PRIVATE LIMITED",
    },
    {
        "id": "9",
        "title": "Haldiram's Aalo Bhujia",
        "images": [
            "https://cdn.grofers.com/cdn-cgi/image/f=auto,fit=scale-down,q=50,metadata=none,w=135/app/images/products/sliding_image/52659a.jpg?ts=1596796010"],
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "brand": "Haldiram",
        "quantity": {
            count: 1,
            unit: "Kg"
        },
        "price": 290,
        "discount": 0.3,
        "category": "Snacks and Munchie",
        "keyFeatures": [
            "Lip smacking flavour",
        ],
        "shelfLife": "6 months",
        "manufacturerDetails": "Kaira District Co-operative Milk Producers' Union Limited, Anand 388 001. At Food Complex Mogar, Mogar. Lic. No. - 10014021001010",
        "license_FSSAILicense": "10012021000071",
        "expiryDate": "Please refer to the packaging of the product for expiry date",
        "seller": "LA SUPER RETAIL PRIVATE LIMITED",
    },
    {
        "id": "10",
        "title": "Britannia Milk Bikis Biscuit",
        "images": [
            "https://cdn.grofers.com/cdn-cgi/image/f=auto,fit=scale-down,q=50,metadata=none,w=135/app/images/products/full_screen/pro_405601.jpg?ts=1655472060"],
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "brand": "Britannia",
        "quantity": {
            count: 500,
            unit: "g"
        },
        "price": 95,
        "discount": 0.15,
        "category": "Snacks and Munchie",
        "keyFeatures": [
            "100% Vegetarian",
            "Made with 100% whole wheat grains",
            "No trans fat",
        ],
        "shelfLife": "8 months",
        "manufacturerDetails": "Kaira District Co-operative Milk Producers' Union Limited, Anand 388 001. At Food Complex Mogar, Mogar. Lic. No. - 10014021001010",
        "license_FSSAILicense": "10012021000071",
        "expiryDate": "Please refer to the packaging of the product for expiry date",
        "seller": "LA SUPER RETAIL PRIVATE LIMITED",
    },

    {
        "id": "11",
        "title": "Onion",
        "images": [
            "https://www.bigbasket.com/media/uploads/p/l/10000150_19-fresho-onion.jpg?tr=w-384,q=80"
        ],
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "brand": "Freshno",
        "quantity": {
            count: 1000,
            unit: "g"
        },
        "price": 34,
        "discount": 0,
        "category": "Vegetables",
        "keyFeatures": [
            "Healthy and fresh",
        ],
        "shelfLife": "15 Days",
        "manufacturerDetails": "Kaira District Co-operative Milk Producers' Union Limited, Anand 388 001. At Food Complex Mogar, Mogar. Lic. No. - 10014021001010",
        "license_FSSAILicense": "10012021000071",
        "expiryDate": "Please refer to the packaging of the product for expiry date",
        "seller": "LA SUPER RETAIL PRIVATE LIMITED",
    },
];



const initialState = {
    products: staticProductsData,
};

const productsReducer = (state = initialState, action) => {
    let updatedState;
    switch (action.type) {

        case LOAD_PRODUCT_DATA:
            updatedState = {
                ...state,
                products: action.payload.products,

            }
            return updatedState;
        default:
            return state;
    };
}

export default productsReducer;