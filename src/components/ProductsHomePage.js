import React, { Component } from 'react';
import "../styles/ProductsHomePage.css";
import ProductCard from './ProductCard';
import { connect } from 'react-redux';
import Footer from './Footer';

class ProductsHomePage extends Component {
    constructor(props) {
        super(props);

        this.homepageImages = [
            "https://cdn.grofers.com/cdn-cgi/image/f=auto,fit=scale-down,q=50,metadata=none,w=360/layout-engine/2023-02/HFS-Web-Banner-1.jpg",

            "https://cdn.grofers.com/cdn-cgi/image/f=auto,fit=scale-down,q=50,metadata=none,w=360/layout-engine/2022-08/PASS_0.jpg",

            "https://cdn.grofers.com/cdn-cgi/image/f=auto,fit=scale-down,q=50,metadata=none,w=360/layout-engine/2022-12/WImported-grocery-banner_WEB.jpg",

            "https://cdn.grofers.com/cdn-cgi/image/f=auto,fit=scale-down,q=50,metadata=none,w=360/layout-engine/2022-06/morning-banner.jpg",

            "https://cdn.grofers.com/cdn-cgi/image/f=auto,fit=scale-down,q=50,metadata=none,w=360/layout-engine/2022-11/Winter_FB-masthead-WEB.png",

            "https://cdn.grofers.com/cdn-cgi/image/f=auto,fit=scale-down,q=50,metadata=none,w=360/layout-engine/2022-10/gifting-store-Banner-Web.jpg",

            // "https://cdn.grofers.com/cdn-cgi/image/f=auto,fit=scale-down,q=50,metadata=none,w=360/layout-engine/2022-11/Winter_HH-masthead-WEB.png",
        ];
    }



    render() {
        return (
            <div className="container-fluid row justify-content-center products-HomePage">
                <div className="mt-4 col-11">
                    <img src="https://cdn.grofers.com/cdn-cgi/image/f=auto,fit=scale-down,q=50,metadata=none,w=1440/layout-engine/2022-05/Group-33704.jpg"
                        className="img-fluid" alt="Responsive image" />
                </div>

                <div className="mt-4 col-11 d-flex flex-nowrap">

                    {
                        this.homepageImages.map((imageSrc, index) => {
                            return <div key={imageSrc + index}
                                className="m-auto col-2">
                                <img src={imageSrc}
                                    className="img-fluid" alt="Responsive image" />
                            </div>
                        })
                    }

                </div>

                <div className="mt-4 col-11 row ">
                    {
                        (Array.isArray(this.props.productData) && this.props.productData.length > 0) &&

                        this.props.productData.map((singleProductObject) => {

                            return (
                                <div key={singleProductObject.id}
                                    className="m-auto col-3">
                                    <ProductCard productData={singleProductObject} />
                                </div>
                            )
                        })


                    }

                </div>

                <Footer />
            </div>

        );
    }
}

const mapStateToProps = (state) => {

    const productDataFromStore = state.productsReducer.products;
    return {
        productData: productDataFromStore,
    };
};

export default connect(mapStateToProps)(ProductsHomePage);
