import React, { Component } from 'react';
import { connect } from 'react-redux';
import "../styles/MyCart.css"
import ProductCardinCart from './ProductCardinCart';

class MyCart extends Component {

    render() {


        let cartHandlingCharge = this.props.cartTotalItems > 3 ? 0 : 2;
        let cartDeliveryCharge = this.props.cartTotalPrice > 250 ? 0 : 35;
        let cartGrandTotal = this.props.cartTotalPrice + cartDeliveryCharge + cartHandlingCharge;
        let cartProductIds = Object.keys(this.props.cartData)
            .filter((key) => {
                if (key === "cartTotalItems" || key === "cartTotalPrice") {
                    return false;
                } else {
                    return true;
                }
            });

        let productsInCart = this.props.productData.filter((singleProductObject) => {
            return (cartProductIds.includes(singleProductObject.id));

        });


        return (
            <>
                <div
                    className="btn btn-success col-lg-1 col-1 d-flex justify-content-between align-items-center ml-1"
                    data-toggle="modal"
                    data-target="#myCartModal">
                    <div>
                        <i className="fa-solid fa-cart-shopping">&nbsp;&nbsp;</i>
                    </div>
                    {
                        this.props.cartTotalItems === 0 ?
                            <div>
                                My Cart
                            </div>
                            :
                            <div className="d-flex flex-column ">
                                <p className="m-0 item-count">{this.props.cartTotalItems}&nbsp;</p>
                                <p className="m-0">&#8377; {this.props.cartTotalPrice}</p>
                            </div>

                    }
                </div>

                <div className="modal fade drawer right-align"
                    id="myCartModal"
                    tabIndex="-1"
                    role="dialog"
                    aria-labelledby="myCartModalLabel"
                    aria-hidden="true">
                    <div className="modal-dialog"
                        role="document">
                        <div className="modal-content d-flex flex-column">
                            <div className="modal-header row align-self-center w-100">
                                <h5 className="modal-title col-8"
                                    id="myCartModalLabel">
                                    My Cart
                                </h5>
                                <button type="button"
                                    className="close col-3"
                                    data-dismiss="modal"
                                    aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            {
                                this.props.cartTotalItems === 0 &&
                                <p className="modal-body align-self-center border-none  p-auto m-auto"
                                >
                                    Cart is empty, Continue Shopping!
                                </p>
                            }

                            {this.props.cartTotalItems !== 0 &&
                                <div className="modal-body col-10 row align-self-center">

                                    <div className="col-12 delivery-time-summary row">
                                        <i className="fa-solid fa-stopwatch col-3 watch-icon"></i>
                                        <span className="col-9 d-flex flex-column">
                                            <h6>Delivery in 9 minutes</h6>
                                            <span>{this.props.cartTotalItems}{" Items"}</span>
                                        </span>
                                    </div>
                                    <div className="col-12">
                                        <div className="row">
                                            {
                                                (Array.isArray(productsInCart) && productsInCart.length > 0) &&

                                                productsInCart.map((singleProductObject) => {

                                                    return (
                                                        <ProductCardinCart
                                                            key={singleProductObject.id}
                                                            productData={singleProductObject} />
                                                    )
                                                })
                                            }
                                        </div>
                                    </div>
                                    <div className="col-12 price-breakdown">
                                        <ul className="list-unstyled">
                                            <li className="d-flex justify-content-between">
                                                <span>MRP</span>
                                                <span>&#8377; {this.props.cartTotalPrice}</span>
                                            </li>
                                            <li className="d-flex justify-content-between">
                                                <span>Handling charge</span>
                                                {cartHandlingCharge === 0 ?
                                                    <span>
                                                        <span className="crossed-out">&#8377;2</span>
                                                        Free</span>
                                                    :
                                                    <span>&#8377; {cartHandlingCharge}</span>
                                                }
                                            </li>
                                            <li className="d-flex justify-content-between">
                                                <span>Delivery charge</span>
                                                {cartDeliveryCharge === 0 ?
                                                    <span>
                                                        <span className="crossed-out">&#8377;35</span>
                                                        Free</span>
                                                    :
                                                    <span>&#8377; {cartDeliveryCharge}</span>
                                                }

                                            </li>
                                            <li className="d-flex justify-content-between">
                                                <h6>Grand Total</h6>
                                                <h6>&#8377; {cartGrandTotal}</h6>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            }

                            {this.props.cartTotalItems !== 0 &&
                                <div className="modal-footer col">
                                    <div type="button"
                                        className="btn btn-success"
                                        data-toggle={this.props.isLoggedIn === false ?
                                            "modal"
                                            :
                                            ""
                                        }
                                        data-target={this.props.isLoggedIn === false ?
                                            "#logInModalCenter"
                                            :
                                            ""
                                        }
                                        onClick={() => {
                                            if(this.props.isLoggedIn === true){
                                                console.log("should take to cart checkout")
                                            }else{
                                                console.log("should take to login then to cart checkout")
                                            }
                                        }
                                        }>
                                        {this.props.cartTotalItems}{" Items "}&#8500;{" "}&#8377;{" "}{cartGrandTotal}{"   "}Proceed {'>'}
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </div>

            </>

        );
    }
}

const mapStateToProps = (state) => {

    const productDataFromStore = state.productsReducer.products;
    const cartDataFromStore = state.cartReducer.cartItems;
    const cartTotalItems = state.cartReducer.cartItems.cartTotalItems;
    const cartTotalPrice = state.cartReducer.cartItems.cartTotalPrice;
    const deliveryTime = state.locationReducer.location.deliveryTime;
    const isLoggedIn = state.userReducer.user.isLoggedIn;

    return {
        cartData: cartDataFromStore,
        productData: productDataFromStore,
        cartTotalItems,
        cartTotalPrice,
        deliveryTime,
        isLoggedIn,
    };
};

export default connect(mapStateToProps)(MyCart);