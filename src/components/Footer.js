import React, { Component } from 'react';

class Footer extends Component {

    render() {
        return (
            <footer class="text-center text-white" >
                <div class="container pt-4">
                    <section class="mb-4">
                        <p className="text-dark p-3">
                            “Blinkit” is owned & managed by "Blink Commerce Private Limited" (formerly known as Grofers India Private Limited) and is not related, linked or interconnected in whatsoever manner or nature, to “GROFFR.COM” which is a real estate services business operated by “Redstone Consultancy Services Private Limited”.
                        </p>

                        <div className="FooterLinks-main-div">
                            <div>
                                <div className="FooterLinks">
                                <h3 className="text-dark">Useful Links</h3>
                                </div>
                                <ul className="FooterLinks d-flex justify-content-between list-unstyled m-auto">
                                    <li className="li-class p-1 text-dark m-auto">
                                        <ul className="list-unstyled" type="col">
                                            <li className="li-class p-1"><a href="" target="_blank" rel="noopener noreferrer" >About</a></li>
                                            <li className="li-class p-1"><a href="" target="_blank" rel="noopener noreferrer nofollow" >Careers</a></li>
                                            <li className="li-class p-1"><a href="" target="_blank" rel="noopener noreferrer" >Blog</a></li>
                                            <li className="li-class p-1"><a href="" target="_blank" rel="noopener noreferrer" >Press</a></li>
                                            <li className="li-class p-1"><a href="" target="_blank" rel="noopener noreferrer" >Lead</a></li>
                                            <li className="li-class p-1"><a href="" target="_blank" rel="noopener noreferrer" >Value</a></li>
                                        </ul>
                                    </li>
                                    <li className="li-class p-1 text-dark m-auto">
                                        <ul className="list-unstyled" type="col">
                                            <li className="li-class p-1"><a href="" target="_blank" rel="noopener noreferrer nofollow" >Privacy</a></li>
                                            <li className="li-class p-1"><a href="" target="_blank" rel="noopener noreferrer nofollow" >Terms</a></li>
                                            <li className="li-class p-1"><a href="" target="_blank" rel="noopener noreferrer nofollow" >FAQs</a></li>
                                            <li className="li-class p-1"><a href="" target="_blank" rel="noopener noreferrer nofollow" >Security</a></li>
                                            <li className="li-class p-1"><a href="" target="_blank" rel="noopener noreferrer nofollow" >Mobile</a></li>
                                            <li className="li-class p-1"><a href="" target="_blank" rel="noopener noreferrer nofollow" >Contact</a></li>
                                        </ul>
                                    </li>
                                    <li className="li-class p-1 text-dark m-auto">
                                        <ul className="list-unstyled" type="col">
                                            <li className="li-class p-1"><a href="" target="_blank" rel="noopener noreferrer nofollow" >Partner</a></li>
                                            <li className="li-class p-1"><a href="" target="_blank" rel="noopener noreferrer nofollow" >Express</a></li>
                                            <li className="li-class p-1"><a href="" target="_blank" rel="noopener noreferrer nofollow" >Seller</a></li>
                                            <li className="li-class p-1"><a href="" target="_blank" rel="noopener noreferrer nofollow" >Spotlight</a></li>
                                            <li className="li-class p-1"><a href="" target="_blank" rel="noopener noreferrer nofollow" >Warehouse</a></li>
                                            <li className="li-class p-1"><a href="" target="_blank" rel="noopener noreferrer nofollow" >Deliver</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div>
                                <div className="FooterLinks">
                                <h3 className="text-dark">Categories</h3>
                                </div>
                                <ul className="FooterLinks  d-flex list-unstyled flex-wrap justify-content-between">
                                    <li className="li-class p-1"><a href="">Vegetables &amp; Fruits</a></li>
                                    <li className="li-class p-1"><a href="">Dairy &amp; Breakfast</a></li>
                                    <li className="li-class p-1"><a href="">Munchies</a></li>
                                    <li className="li-class p-1"><a href="">Cold Drinks &amp; Juices</a></li>
                                    <li className="li-class p-1"><a href="">Instant &amp; Frozen Food</a></li>
                                    <li className="li-class p-1"><a href="">Tea, Coffee &amp; Health Drinks</a></li>
                                    <li className="li-class p-1"><a href="">Bakery &amp; Biscuits</a></li>
                                    <li className="li-class p-1"><a href="">Sweet Tooth</a></li>
                                    <li className="li-class p-1"><a href="">Atta, Rice &amp; Dal</a></li>
                                    <li className="li-class p-1"><a href="">Dry Fruits, Masala &amp; Oil</a></li>
                                    <li className="li-class p-1"><a href="">Sauces &amp; Spreads</a></li>
                                    <li className="li-class p-1"><a href="">Chicken, Meat &amp; Fish</a></li>
                                    <li className="li-class p-1"><a href="">Paan Corner</a></li>
                                    <li className="li-class p-1"><a href="">Organic &amp; Premium</a></li>
                                    <li className="li-class p-1"><a href="">Baby Care</a></li>
                                    <li className="li-class p-1"><a href="">Pharma &amp; Wellness</a></li>
                                    <li className="li-class p-1"><a href="">Cleaning Essentials</a></li>
                                    <li className="li-class p-1"><a href="">Home &amp; Office</a></li>
                                    <li className="li-class p-1"><a href="">Personal Care</a></li>
                                    <li className="li-class p-1"><a href="">Pet Care</a></li>
                                </ul>
                            </div>
                        </div>
                        <div className="FooterLinks">
                            <h3 className="text-dark">Brands</h3>
                        </div>
                        <ul className="FooterLinks d-flex list-unstyled flex-wrap justify-content-between">
                            <li className="li-class p-1"><a href="">grofers Happy Day</a></li>
                            <li className="li-class p-1"><a href="">grofers Happy Home</a></li>
                            <li className="li-class p-1"><a href="">grofers Mother's Choice</a></li>
                            <li className="li-class p-1"><a href="">g Fresh</a></li>
                            <li className="li-class p-1"><a href="">O'range</a></li>
                            <li className="li-class p-1"><a href="">Savemore</a></li>
                            <li className="li-class p-1"><a href="">24 Mantra</a></li>
                            <li className="li-class p-1"><a href="">Aashirvaad</a></li>
                            <li className="li-class p-1"><a href="">Act II</a></li>
                            <li className="li-class p-1"><a href="">Amul</a></li>
                            <li className="li-class p-1"><a href="">Axe</a></li>
                            <li className="li-class p-1"><a href="">Bambino</a></li>
                            <li className="li-class p-1"><a href="">Best Value</a></li>
                            <li className="li-class p-1"><a href="">Bingo</a></li>
                            <li className="li-class p-1"><a href="">Bisleri</a></li>
                            <li className="li-class p-1"><a href="">Boost</a></li>
                            <li className="li-class p-1"><a href="">Bournvita</a></li>
                            <li className="li-class p-1"><a href="">Britannia</a></li>
                            <li className="li-class p-1"><a href="">Brooke Bond</a></li>
                            <li className="li-class p-1"><a href="">Bru</a></li>
                            <li className="li-class p-1"><a href="">Cadbury</a></li>
                            <li className="li-class p-1"><a href="">Cheetos</a></li>
                            <li className="li-class p-1"><a href="">Cinthol</a></li>
                            <li className="li-class p-1"><a href="">Closeup</a></li>
                            <li className="li-class p-1"><a href="">Coca-Cola</a></li>
                            <li className="li-class p-1"><a href="">Colgate</a></li>
                            <li className="li-class p-1"><a href="">Dabur</a></li>
                            <li className="li-class p-1"><a href="">Danone</a></li>
                            <li className="li-class p-1"><a href="">Del Monte</a></li>
                            <li className="li-class p-1"><a href="">Dettol</a></li>
                            <li className="li-class p-1"><a href="">Dhara</a></li>
                            <li className="li-class p-1"><a href="">Dove</a></li>
                            <li className="li-class p-1"><a href="">Durex</a></li>
                            <li className="li-class p-1"><a href="">English Oven</a></li>
                            <li className="li-class p-1"><a href="">Everest</a></li>
                            <li className="li-class p-1"><a href="">Fiama Di Wills</a></li>
                            <li className="li-class p-1"><a href="">Garnier</a></li>
                            <li className="li-class p-1"><a href="">Gatorade</a></li>
                            <li className="li-class p-1"><a href="">Gillette</a></li>
                            <li className="li-class p-1"><a href="">Glucon-D</a></li>
                            <li className="li-class p-1"><a href="">Grocery</a></li>
                            <li className="li-class p-1"><a href="">Gowardhan</a></li>
                            <li className="li-class p-1"><a href="">Hajmola</a></li>
                            <li className="li-class p-1"><a href="">Haldiram's</a></li>
                            <li className="li-class p-1"><a href="">Head &amp; Shoulders</a></li>
                            <li className="li-class p-1"><a href="">Heinz</a></li>
                            <li className="li-class p-1"><a href="">Himalaya</a></li>
                            <li className="li-class p-1"><a href="">Horlicks</a></li>
                            <li className="li-class p-1"><a href="">India Gate</a></li>
                            <li className="li-class p-1"><a href="">Kellogg's</a></li>
                            <li className="li-class p-1"><a href="">Kinley</a></li>
                            <li className="li-class p-1"><a href="">Kissan</a></li>
                            <li className="li-class p-1"><a href="">Knorr</a></li>
                            <li className="li-class p-1"><a href="">L'Oreal</a></li>
                            <li className="li-class p-1"><a href="">Lay's</a></li>
                            <li className="li-class p-1"><a href="">Lijjat</a></li>
                            <li className="li-class p-1"><a href="">Limca</a></li>
                            <li className="li-class p-1"><a href="">Lipton</a></li>
                            <li className="li-class p-1"><a href="">Maggi</a></li>
                            <li className="li-class p-1"><a href="">Madhur</a></li>
                            <li className="li-class p-1"><a href="">McCain</a></li>
                            <li className="li-class p-1"><a href="">MDH</a></li>
                            <li className="li-class p-1"><a href="">Minute Maid</a></li>
                            <li className="li-class p-1"><a href="">Mirinda</a></li>
                            <li className="li-class p-1"><a href="">Mother Dairy</a></li>
                            <li className="li-class p-1"><a href="">Mountain Dew</a></li>
                            <li className="li-class p-1"><a href="">MTR</a></li>
                            <li className="li-class p-1"><a href="">Nescafe</a></li>
                            <li className="li-class p-1"><a href="">Nestle</a></li>
                            <li className="li-class p-1"><a href="">Nivea</a></li>
                            <li className="li-class p-1"><a href="">Nutella</a></li>
                            <li className="li-class p-1"><a href="">Oral-B</a></li>
                            <li className="li-class p-1"><a href="">Oreo</a></li>
                            <li className="li-class p-1"><a href="">Palmolive</a></li>
                            <li className="li-class p-1"><a href="">Pantene</a></li>
                            <li className="li-class p-1"><a href="">Paper Boat</a></li>
                            <li className="li-class p-1"><a href="">Parachute</a></li>
                            <li className="li-class p-1"><a href="">Parle</a></li>
                            <li className="li-class p-1"><a href="">Patanjali</a></li>
                            <li className="li-class p-1"><a href="">Pears</a></li>
                            <li className="li-class p-1"><a href="">Pepsi</a></li>
                            <li className="li-class p-1"><a href="">Pepsodent</a></li>
                            <li className="li-class p-1"><a href="">Pillsbury</a></li>
                            <li className="li-class p-1"><a href="">Princeware</a></li>
                            <li className="li-class p-1"><a href="">Rajdhani</a></li>
                            <li className="li-class p-1"><a href="">Real</a></li>
                            <li className="li-class p-1"><a href="">Red Bull</a></li>
                            <li className="li-class p-1"><a href="">Safal</a></li>
                            <li className="li-class p-1"><a href="">Saffola</a></li>
                            <li className="li-class p-1"><a href="">Shakti Bhog</a></li>
                            <li className="li-class p-1"><a href="">Smith &amp; Jones</a></li>
                            <li className="li-class p-1"><a href="">Sprite</a></li>
                            <li className="li-class p-1"><a href="">Stayfree</a></li>
                            <li className="li-class p-1"><a href="">Sundrop</a></li>
                            <li className="li-class p-1"><a href="">Sunfeast</a></li>
                            <li className="li-class p-1"><a href="">Sunsilk</a></li>
                            <li className="li-class p-1"><a href="">Taj Mahal</a></li>
                            <li className="li-class p-1"><a href="">Tang</a></li>
                            <li className="li-class p-1"><a href="">Tata sampann</a></li>
                            <li className="li-class p-1"><a href="">Tata tea</a></li>
                            <li className="li-class p-1"><a href="">Tetley</a></li>
                            <li className="li-class p-1"><a href="">Thums Up</a></li>
                            <li className="li-class p-1"><a href="">Tropicana</a></li>
                            <li className="li-class p-1"><a href="">Twinings</a></li>
                            <li className="li-class p-1"><a href="">Uncle Chipps</a></li>
                            <li className="li-class p-1"><a href="">Unibic</a></li>
                            <li className="li-class p-1"><a href="">Vaseline</a></li>
                            <li className="li-class p-1"><a href="">Veet</a></li>
                            <li className="li-class p-1"><a href="">Wagh Bakri</a></li>
                            <li className="li-class p-1"><a href="">Wai Wai</a></li>
                            <li className="li-class p-1"><a href="">Whisper</a></li>
                            <li className="li-class p-1"><a href="">Whole Farm</a></li>
                        </ul>

                    </section>

                </div>

                <div class="container pt-4">
                    <section class="mb-4">
                        <a
                            class="btn btn-link btn-floating btn-lg text-dark m-1"
                            href=""
                            role="button"
                            data-mdb-ripple-color="dark"
                        ><i class="fab fa-facebook-f"></i
                        ></a>


                        <a
                            class="btn btn-link btn-floating btn-lg text-dark m-1"
                            href=""
                            role="button"
                            data-mdb-ripple-color="dark"
                        ><i class="fab fa-twitter"></i
                        ></a>


                        <a
                            class="btn btn-link btn-floating btn-lg text-dark m-1"
                            href=""
                            role="button"
                            data-mdb-ripple-color="dark"
                        ><i class="fab fa-google"></i
                        ></a>


                        <a
                            class="btn btn-link btn-floating btn-lg text-dark m-1"
                            href=""
                            role="button"
                            data-mdb-ripple-color="dark"
                        ><i class="fab fa-instagram"></i
                        ></a>


                        <a
                            class="btn btn-link btn-floating btn-lg text-dark m-1"
                            href=""
                            role="button"
                            data-mdb-ripple-color="dark"
                        ><i class="fab fa-linkedin"></i
                        ></a>

                    </section>

                </div>



                <div class="text-center text-dark p-3">
                    <a class="text-dark" href="">{" "}© Blink Commerce Private Limited (formerly known as Grofers India Private Limited), 2016-2023</a>
                </div>

            </footer>
        );
    }
}

export default Footer;
