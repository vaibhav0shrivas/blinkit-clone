import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import './App.css';

import Navbar from './components/Navbar';
import ProductsHomePage from './components/ProductsHomePage';
import SearchResult from './components/SearchResult';


class App extends Component {

  render() {
    return (
      <Router>
        <div className="App container-fluid">
          <Route
            path="*"
            render={(props) => {
              return <Navbar {...props} />
            }}
          />

          <Switch>
            <Route
              exact path="/"
              render={(props) => {
                return <ProductsHomePage />
              }}
            />
          </Switch>
          <Switch>
            <Route
              exact path="/s*"
              render={(props) => {
                return <SearchResult />
              }}
            />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;

